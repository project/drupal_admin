# Changelog

## Drupal Admin 1.0.0-beta3, 2023-08-21

Changes since 1.0.0-beta2:

- Issue #3382385: Allow a default role to be passed to the login endpoint.

## Drupal Admin 1.0.0-beta2, 2023-08-12

Changes since 1.0.0-beta1:

- Issue #3372912: Fix the issues reported by phpcs
- Issue #3380834: hook_help function name is wrong

## Drupal Admin 1.0.0-beta1, 2023-06-26

- Initial implementation for Drupal 9+
