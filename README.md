# Drupal Admin

The Drupal Admin module allows the site (called a spoke) to integrate with [Drupal Admin](https://www.drupaladmin.com/) (called the hub). Once installed and configured, this module will allow the users that are logged into the hub to login to the sites using a specified role via single-sign-on (SSO). Which sites a user can login to is controlled by which users are added to which sites in the hub.

Create a hub by signing up at [Drupal Admin](https://www.drupaladmin.com/).

For a full description of the module, visit the [project page](https://www.drupal.org/project/drupal_admin).

Submit bug reports and feature suggestions, or track changes in the [issue queue](https://www.drupal.org/project/issues/drupal_admin).

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## Requirements

- This module requires the openssl_verify() PHP function, which is part of the [OpenSSL library](https://www.php.net/manual/en/book.openssl.php) for PHP.

- This module requires an account on [Drupal Admin](https://www.drupaladmin.com/).

## Recommended modules

- [Markdown filter](https://www.drupal.org/project/markdown): When enabled, display of the project's README.md help will be rendered with markdown.
- [Config override warn](https://www.drupal.org/project/config_override_warn): When enabled, allows notification of overriden config values (in one of the settings.php files) in non-production environments.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the module at Administration > Extend. This will create an API key at Configuration > Drupal Admin > Authentication. None of the fields on this form can be edited through the UI. The 'Enabled the Handshake Endpoint' checkbox should be checked.
1. Select the roles that should be handled by the hub's SSO functionality at Configuration > Drupal Admin > Roles.
1. Add the site to the [Drupal Admin](https://www.drupaladmin.com/) hub, setting the API key that was generated from step 1.
1. In the hub, use the 'Connect' operation to allow the hub to handshake with the site. Once this is complete, settings on Configuration > Drupal > Drupal Admin > Authentication will be updated - 'Private Key' will be populated, 'Site ID' will be populated and the 'Enabled the Handshake Endpoint' checkbox will be unchecked.
1. To enable all Drupal Admin functionality, the final step is to check the 'Enable all of the Drupal Admin functionality.' checkbox on Configuration > Drupal Admin > Settings.

The Drupal core roles of 'Anonymous' and 'Authenticated' are not available on the Configuration > Drupal Admin > Roles form. Anonymous users do not need to login and all users that are created will also get the 'Authenticated' role. Drupal Admin is meant to be used with defined site roles.

## Maintainers

- Scott Joudry - [slydevil](https://www.drupal.org/u/slydevil)
