<?php

namespace Drupal\drupal_admin\Utility;

/**
 * API Key Utility class.
 */
class ApiKey {

  /**
   * String length.
   *
   * The length of the generated API key.
   *
   * @var int
   */
  protected static int $stringLength = 255;

  /**
   * String characters.
   *
   * The characters that will be used to generate the API key.
   *
   * @var string
   */
  protected static string $stringCharacters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

  /**
   * Generate API key.
   *
   * Generate a random API key.
   *
   * @return string
   *   The generated API key.
   */
  public static function generate() {
    $key = '';

    $seed = str_split(self::$stringCharacters);
    $max = count($seed) - 1;

    for ($i = 0; $i < self::$stringLength; $i++) {
      $key .= $seed[random_int(0, $max)];
    }

    return $key;
  }

}
