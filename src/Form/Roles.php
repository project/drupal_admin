<?php

namespace Drupal\drupal_admin\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form for add/edit forms.
 */
class Roles extends ConfigFormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupal_admin_roles_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('drupal_admin.settings');

    $role_entities = $this->entityTypeManager->getStorage('user_role')
      ->loadMultiple();
    $roles = [];
    /** @var \Drupal\user\Entity\Role $role */
    foreach ($role_entities as $role_id => $role) {
      if ($role_id != 'anonymous' && $role_id != 'authenticated') {
        $roles[$role_id] = $role->label();
      }
    }

    $form['role_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Roles'),
    ];
    $form['role_wrapper']['roles'] = [
      '#type' => 'checkboxes',
      '#title_display' => 'invisible',
      '#title' => $this->t('Roles'),
      '#description' => $this->t('Select which roles should have SSO login enabled from the Drupal Admin UI. All roles not selected will still be able to login using the site login form.'),
      '#options' => $roles,
      '#default_value' => $config->get('roles'),
    ];
    $form['allow_local'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow local accounts to login (for managed roles above).'),
      '#default_value' => $config->get('allow_local'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();
    $config = $this->config('drupal_admin.settings');
    $config->set('allow_local', $values['allow_local']);
    $config->set('roles', array_keys($values['roles']));
    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['drupal_admin.settings'];
  }

}
