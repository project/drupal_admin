<?php

namespace Drupal\drupal_admin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base form for add/edit forms.
 */
class Authentication extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupal_admin_auth_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('drupal_admin.settings');

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => 'The following data is for informational purposes and none of these values can be modified here.',
    ];
    $form['api_key'] = [
      '#type' => 'textarea',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('The API key is generated on module install. This API key is only used during the handshake process between the Drupal Admin UI and this site. Once the handshake is complete the public key below is used during communication between the two. This value can be regenerated using the action button above.'),
      '#disabled' => TRUE,
      '#default_value' => $config->get('api_key'),
      '#rows' => 5,
    ];
    $form['public_key'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Public Key'),
      '#description' => $this->t('The public key is set during the handshake process between the Drupal Admin UI and this site and is used for all communication between this site and the Drupal Admin UI.'),
      '#disabled' => TRUE,
      '#default_value' => $config->get('public_key'),
      '#rows' => 10,
    ];
    $form['site_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site ID'),
      '#description' => $this->t('The site ID is the unique identifier for this site in the Drupal Admin UI and is using during communication between the UI and this site. This value is set during the handshake process between the two.'),
      '#disabled' => TRUE,
      '#default_value' => $config->get('site_id'),
    ];
    $form['handshake_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable the handshake endpoint.'),
      '#description' => $this->t('Once the handshake is done, the handshake endpoint is disabled (for security purposes). This value can be enabled with drush or by using a config override value in settings.php. Only enable if a new handshake is required.'),
      '#disabled' => TRUE,
      '#default_value' => $config->get('handshake_enabled'),
    ];

    $form['actions']['submit']['#access'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['drupal_admin.settings'];
  }

}
