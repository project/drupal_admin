<?php

namespace Drupal\drupal_admin\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\drupal_admin\Utility\ApiKey;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Sites controller class.
 */
class Api extends ControllerBase {

  /**
   * Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Time Manager.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $timeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    $instance->requestStack = $container->get('request_stack');
    $instance->timeManager = $container->get('datetime.time');

    return $instance;
  }

  /**
   * Generate new API key.
   */
  public function generate() {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->configFactory->getEditable('drupal_admin.settings');
    $config->set('api_key', ApiKey::generate());
    $config->save();

    $this->messenger()->addMessage($this->t('New API key generated successfully.'));

    return $this->redirect('drupal_admin.config.settings');
  }

  /**
   * Handle handshake.
   */
  public function handleHandshake() {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->configFactory->getEditable('drupal_admin.settings');

    // Only proceed if handshakes are enabled.
    if (!$config->get('handshake_enabled')) {
      return $this->errorResponseHandshake();
    }

    if (!$this->validateMethod(['POST'])) {
      return $this->errorResponseMethod();
    }

    if (!$this->validateApiKey()) {
      return $this->errorResponseKey();
    }

    $body = $this->requestStack->getCurrentRequest()->getContent();
    if ($errors = $this->validateBody($body, ['public_key', 'site_id'])) {
      return $this->errorResponseBody($errors);
    }

    // Set config values.
    $body = json_decode($body);
    $config->set('public_key', $body->public_key);
    $config->set('site_id', $body->site_id);
    $config->set('handshake_enabled', FALSE);
    $config->save();

    $output = [
      'roles' => $this->getRoles(),
    ];
    return $this->prepareResponse($output);
  }

  /**
   * Handle login.
   */
  public function handleLogin() {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->configFactory->getEditable('drupal_admin.settings');

    // Only proceed with the login if the module functionality is enabled.
    if (!$config->get('enabled')) {
      return $this->errorResponseLogin();
    }

    if (!$this->validateMethod(['GET', 'POST'])) {
      return $this->errorResponseMethod();
    }

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $this->requestStack->getCurrentRequest();
    switch ($request->getMethod()) {

      // The initial request will hit this URL, which will redirect immediately
      // to the Drupal Admin UI for user login/validation.
      case 'GET':
        return new TrustedRedirectResponse('https://www.drupaladmin.com/sso/request?site=' . $config->get('site_id'));

      // Once the user has logged into the Drupal Admin UI and is validated, the
      // UI will post a signed message to the site, signalling that the user is
      // good to go.
      case 'POST':

        // Message is required.
        $message = $request->request->get('message');
        if (!$message) {
          return $this->errorResponseMessage();
        }

        // Message needs to be base64 decoded.
        $message = base64_decode($message);
        if (!$message) {
          return $this->errorResponseMessage();
        }

        if ($errors = $this->validateBody($message, ['data', 'signature'])) {
          return $this->errorResponseBody($errors);
        }

        $message = json_decode($message);
        $data = json_encode($message->data);
        $properties = ['email', 'role', 'created', 'expiry'];
        if ($errors = $this->validateBody($data, $properties)) {
          return $this->errorResponseBody($errors);
        }

        $signature = base64_decode($message->signature);
        if (!$signature || !$this->validateSignature($data, $signature)) {
          return $this->errorResponseSignature();
        }

        if (!$this->validateTimes($message->data)) {
          return $this->errorResponseTimes();
        }

        // Load or create the user.
        /** @var \Drupal\user\Entity\User $user */
        $user = user_load_by_mail($message->data->email);
        if ($user) {

          // Make sure the user is active.
          if (!$user->isActive()) {
            return $this->errorResponseBlocked();
          }

          // If local accounts are allowed, don't change anything about the
          // user (except adding the role below).
          if (!$config->get('allow_local')) {

            // Reset the roles of the user.
            $roles = $user->getRoles(TRUE);
            foreach ($roles as $role) {
              $user->removeRole($role);
            }

            // Reset the password.
            $user->setPassword(ApiKey::generate());
          }
        }
        else {
          /** @var \Drupal\user\Entity\User $user */
          $user = $this->entityTypeManager()
            ->getStorage('user')
            ->create(
              [
                'name' => $message->data->email,
                'mail' => $message->data->email,
                'pass' => ApiKey::generate(),
                'status' => 1,
              ]
            );
        }

        // Default role is "authenticated" and if the default is set (role = 0)
        // then don't set any roles as "authenticated" is implied and always
        // set by Drupal.
        $role = $message->data->role;
        if ($role != 0) {
          $user->addRole($role);
        }
        $user->save();
        user_login_finalize($user);

        // @todo redirect somewhere better.
        return $this->redirect('user.page');

    }
  }

  /**
   * Handle roles.
   */
  public function handleRoles() {
    if (!$this->validateMethod(['GET'])) {
      return $this->errorResponseMethod();
    }

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $this->requestStack->getCurrentRequest();
    if (!$request->headers->has('signature')) {
      $this->errorResponseSignature();
    }

    $body = $this->requestStack->getCurrentRequest()->getContent();
    $signature = base64_decode($request->headers->get('signature'));
    if (!$signature || !$this->validateSignature($body, $signature)) {
      return $this->errorResponseSignature();
    }

    $output = [
      'roles' => $this->getRoles(),
    ];
    return $this->prepareResponse($output);
  }

  /**
   * Generic error response.
   *
   * @param array $error
   *   The body of the error to return.
   * @param int $http_code
   *   The status code to return.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The error response.
   */
  protected function errorResponse(array $error, int $http_code) {
    return new JsonResponse($error, $http_code);
  }

  /**
   * Blocked account error response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The error response.
   */
  protected function errorResponseBlocked() {
    $message = ['message' => 'Unable to login the user since they are blocked on the site.'];
    return $this->errorResponse($message, 403);
  }

  /**
   * Error response if the body is malformed.
   *
   * @param string[] $errors
   *   The list of errors to add to the response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The error response.
   */
  protected function errorResponseBody(array $errors = []) {
    $message = [
      'message' => 'The body of the request is required.',
      'errors' => $errors,
    ];
    return $this->errorResponse($message, 400);
  }

  /**
   * Handshake error response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The error response.
   */
  protected function errorResponseHandshake() {
    $message = [
      'error' => 1,
      'message' => 'Handshake is forbidden.',
    ];
    return $this->errorResponse($message, 200);
  }

  /**
   * API Key error response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The error response.
   */
  protected function errorResponseKey() {
    $message = ['message' => 'The API key is invalid.'];
    return $this->errorResponse($message, 401);
  }

  /**
   * API Key error response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The error response.
   */
  protected function errorResponseLogin() {
    $message = ['message' => 'Login functionality using this method is disabled.'];
    return $this->errorResponse($message, 403);
  }

  /**
   * Message error response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The error response.
   */
  protected function errorResponseMessage() {
    $message = ['message' => 'The message POST variable is required and needs to be base 64 encoded.'];
    return $this->errorResponse($message, 405);
  }

  /**
   * HTTP method error response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The error response.
   */
  protected function errorResponseMethod() {
    $message = ['message' => 'The method is not allowed.'];
    return $this->errorResponse($message, 405);
  }

  /**
   * Signature error response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The error response.
   */
  protected function errorResponseSignature() {
    $message = ['message' => 'The signature is invalid.'];
    return $this->errorResponse($message, 401);
  }

  /**
   * Time error response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The error response.
   */
  protected function errorResponseTimes() {
    $message = ['message' => 'The login request has expired.'];
    return $this->errorResponse($message, 401);
  }

  /**
   * Prepare Response.
   *
   * Converts the array passed in into JSON, sets all cache related meta-data
   * and gzips the response is needed.
   *
   * @param mixed $output
   *   The output to prepare.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response.
   */
  protected function prepareResponse($output) {
    $response = new JsonResponse($output);

    // Compress only if client request allows gzip.
    if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== FALSE) {
      $content = $response->getContent();
      $compressed = gzencode($content, 9, FORCE_GZIP);
      $response->setContent($compressed);
      $response->headers->set('Content-Encoding', 'gzip');
    }

    return $response;
  }

  /**
   * Get roles.
   *
   * @return array
   *   A list of role names, keyed by role id.
   */
  protected function getRoles() {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->configFactory->getEditable('drupal_admin.settings');

    // Return the roles of the site.
    $roles = [];
    if (count($config->get('roles'))) {
      $role_entities = $this->entityTypeManager()
        ->getStorage('user_role')
        ->loadMultiple($config->get('roles'));
      /** @var \Drupal\user\Entity\Role $role */
      foreach ($role_entities as $role_id => $role) {
        $roles[$role_id] = $role->label();
      }
    }

    return $roles;
  }

  /**
   * Validate API key.
   *
   * @return bool
   *   TRUE if the apikey header value matches key and FALSE otherwise.
   */
  protected function validateApiKey() {
    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $this->requestStack->getCurrentRequest();
    if ($request->headers->has('apikey')) {
      $api_key = $this->config('drupal_admin.settings')->get('api_key');
      if ($request->headers->get('apikey') == $api_key) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Validate request body.
   *
   * @param string $body
   *   The body of the request.
   * @param array $properties
   *   The properties required for the body.
   *
   * @return string[]
   *   The array of errors.
   */
  protected function validateBody(string $body = NULL, array $properties = []) {
    $errors = [];

    if (empty($body)) {
      $errors[] = 'Body is missing and is required.';
    }
    else {
      $json_body = @json_decode($body);
      if (json_last_error() != JSON_ERROR_NONE) {
        $errors[] = 'Invalid JSON: ' . json_last_error_msg();
      }

      foreach ($properties as $property) {
        if (!isset($json_body->{$property})) {
          $errors[] = "The '" . $property . "' property is missing";
        }
      }
    }

    return $errors;
  }

  /**
   * Validate HTTP method.
   *
   * @param array $methods
   *   The accepted methods.
   *
   * @return bool
   *   TRUE if the http method matches the method's passed in and FALSE
   *   otherwise.
   */
  protected function validateMethod(array $methods) {
    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $this->requestStack->getCurrentRequest();
    if (in_array($request->getMethod(), $methods)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Validate Signature.
   *
   * @param string $data
   *   The data to validate.
   * @param string $signature
   *   The signature to validate.
   *
   * @return bool
   *   TRUE if the signature header value matches key and FALSE otherwise.
   */
  protected function validateSignature(string $data, string $signature) {
    if (openssl_verify($data, $signature, $this->config('drupal_admin.settings')->get('public_key'), OPENSSL_ALGO_SHA384) == 1) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Validate times.
   *
   * @param object $data
   *   The message data.
   *
   * @return string[]
   *   The array of errors.
   */
  protected function validateTimes(object $data) {

    // These dates will be passed in ISO 8601 date format.
    $created = strtotime($data->created);
    $expiry = strtotime($data->expiry);

    // The request from the Drupal Admin UI must have been created before now
    // and must expire after now.
    $now = $this->timeManager->getCurrentTime();

    if ($created <= $now && $expiry >= $now) {
      return TRUE;
    }

    return FALSE;
  }

}
